package com.rentit.customer.domain.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QUserID is a Querydsl query type for UserID
 */
@Generated("com.mysema.query.codegen.EmbeddableSerializer")
public class QUserID extends BeanPath<UserID> {

    private static final long serialVersionUID = 998333730L;

    public static final QUserID userID = new QUserID("userID");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public QUserID(String variable) {
        super(UserID.class, forVariable(variable));
    }

    public QUserID(Path<? extends UserID> path) {
        super(path.getType(), path.getMetadata());
    }

    public QUserID(PathMetadata<?> metadata) {
        super(UserID.class, metadata);
    }

}

