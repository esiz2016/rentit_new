///*
package com.rentit.security.config;

import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


@Configuration
@Order(1)
public class WebApiSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {
	
	  @Override
	  protected void configure(HttpSecurity http) throws Exception {
	    http.csrf().disable()
	          .antMatcher("/api/**").authorizeRequests()
	          .antMatchers("/api/**").hasRole("SiteEngineer")
	          .antMatchers("/api/**").authenticated()
	          .and().httpBasic()
	          .authenticationEntryPoint((req, resp, exc) ->
	                resp.sendError(HttpServletResponse.SC_UNAUTHORIZED,"Unauthorized: Authentication token was either missing or invalid."));
	
	
	  }

}
//*/