package com.rentit.customer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rentit.customer.domain.model.Authorities;
import com.rentit.customer.domain.model.AuthorityID;
import com.rentit.customer.domain.model.User;
import com.rentit.customer.dto.UserDTO;
import com.rentit.customer.repository.AuthoritiesRepository;
import com.rentit.customer.repository.UserRepository;
import com.rentit.inventory.infrastructure.idgeneration.InventoryIdentifierGenerator;

@Service
public class UserService {
	  @Autowired
	    InventoryIdentifierGenerator identifierGenerator;
	
	@Autowired
	UserRepository UserRepository;
	
	@Autowired 
	AuthoritiesRepository AuthRepo;
	
	public void saveUser(UserDTO userDTO){
		User user= CreateUser(userDTO);
		
		Authorities auth=CreateAuthority(userDTO);
		
		AuthRepo.save(auth);
		UserRepository.save(user);
		
	}

	private Authorities CreateAuthority(UserDTO userDTO) {
	
		AuthorityID authID = identifierGenerator.nextAuthorityID();
		return Authorities.of(authID,
				userDTO.getUserName(), "ROLE_SiteEngineerr");
	}

	private User CreateUser(UserDTO userDTO) {
		
		return User.of(identifierGenerator.nextUserID(), 
					userDTO.getName(), 
					userDTO.getAddress(), 
					userDTO.getPhone(), 
					userDTO.getUserName(), 
					userDTO.getPassword());
		 
	}
	
	public void  setAuthority(User user){
		
		Authorities.of(identifierGenerator.nextAuthorityID(), user.getUsername(), "ROLE_SiteEngineerr");
	}
	

}
