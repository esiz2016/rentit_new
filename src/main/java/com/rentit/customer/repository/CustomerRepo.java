package com.rentit.customer.repository;

import org.springframework.data.jpa.mapping.JpaPersistentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rentit.customer.domain.model.Customer;
import com.rentit.customer.domain.model.CustomerID;

@Repository
public interface CustomerRepo extends JpaRepository<Customer,CustomerID>{

}
