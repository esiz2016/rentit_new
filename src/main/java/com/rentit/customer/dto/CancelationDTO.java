package com.rentit.customer.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor(force = true)
public class CancelationDTO {
	
	Long _id;
	String customerName;
	String customerAddress;
	Long poId;
	Long plantStatus;
	
}
