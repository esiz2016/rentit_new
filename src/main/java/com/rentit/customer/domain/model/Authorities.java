package com.rentit.customer.domain.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED, force = true)
public class Authorities {

	@EmbeddedId
	AuthorityID id;
	
	String username;
	
	String authority;
	       
	
	public static Authorities of(AuthorityID id, String username, String authority) {
		Authorities cu = new Authorities();
		cu.id=id;
		cu.username= username;
		cu.authority= authority;
		
		return cu;
	}
	
	
	
}
