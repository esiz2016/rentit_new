package com.rentit.invoice;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.hateoas.hal.Jackson2HalModule;
import org.springframework.http.HttpMethod;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.http.Http;
import org.springframework.integration.dsl.mail.Mail;
import org.springframework.integration.dsl.support.Transformers;
import org.springframework.integration.support.json.Jackson2JsonObjectMapper;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.bazaarvoice.jolt.Chainr;
import com.bazaarvoice.jolt.JsonUtils;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rentit.inventory.application.dto.PlantInventoryEntryDTO;
import com.rentit.sales.application.service.SalesService;

@Service
@IntegrationComponentScan
public class SendRecieveInvoice {
	
	@Autowired
	RentalServiceGateway gateWays;
	
	@Autowired
	SalesService SalesService;
	
	/*
	@Autowired
	InvoiceProcessor InvoiceProcessor;
    */
	MimeMessage createEmail(String Toemail) throws Exception{
		JavaMailSender mailSender = new JavaMailSenderImpl();
		String invoice1 =
		  "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
		  "<invoice>\n"+
		  "	<purchaseOrderHRef>http://rentit.com/api/sales/orders/1</purchaseOrderHRef>\n"+
		  "	<total>1000.00</total>\n"+
		  "</invoice>\n";

		MimeMessage rootMessage = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(rootMessage, true);
		helper.setFrom("rentit555@gmail.com");
		helper.setTo(Toemail);
		helper.setSubject("Invoice Purchase Order 123");
		helper.setText("Dear customer,\n\nPlease find attached the Invoice corresponding to your Purchase Order 123.\n\nKindly yours,\n\nRentIt Team!");

		helper.addAttachment("invoice-po-123.xml", new ByteArrayDataSource(invoice1, "application/xml"));
		
		return rootMessage;
		
	}
	
	public void sendInvoice(String email) throws Exception{
		MimeMessage msg= createEmail(email);
		gateWays.sendInvoice(msg);
	}
		
	@Bean
	IntegrationFlow flow() {
		try{
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				mapper.registerModule(new Jackson2HalModule());
				
				
			  return IntegrationFlows.from("queryPlantCatalogChannel")
			          .handle(Http.outboundGateway("http://192.168.99.100:3030/rest/plants?name={name}")
			                      .uriVariable("name", "payload.name")
			                      .httpMethod(HttpMethod.GET)
			                      .expectedResponseType(String.class))
			          .<String,String>transform(m -> sirenToHal(m))
			          .transform(Transformers.fromJson(PlantInventoryEntryDTO[].class,
		                      new Jackson2JsonObjectMapper(mapper)))
			          .get();
		}
		catch(Exception e){
			return null;}
     }
	
	@Bean                    //Get Invoice
	IntegrationFlow processInvoiceFlow() {
	try{	
	  return IntegrationFlows
	      .from(Mail.imapIdleAdapter(String.format("imaps://%s:%s@imap.gmail.com/INBOX", "rentit555", "123qaz123"))
	            .selectorExpression("subject matches '.*invoice.*'"))
	      .transform("@invoiceProcessor.extractInvoice(payload)")
	      .route("#xpath(payload, '//total <= 1800', 'string')", mapping -> mapping
	          .subFlowMapping("true", sf -> sf
	              .handle("invoiceProcessor", "processInvoice"))
	          .subFlowMapping("false", sf -> sf
	              .handle(System.out::println))
	      )
	      .get();
	}catch(Exception e){
		return null;
	}
	}
	
	@Bean
	IntegrationFlow sendInvoiceFlow() {    //Send Invoice
	  return IntegrationFlows.from("sendInvoiceChannel")
	      .handle(Mail.outboundAdapter("smtp.gmail.com")
	          .port(465)
	          .protocol("smtps")
	          .credentials("rentit555", "123qaz123")
	          .javaMailProperties(p -> p.put("mail.debug", "false")))
	      .get();
	}
	
	String sirenToHal(String input) {
	    try {
	        Resource spec = new ClassPathResource("specCollection.json", this.getClass());
	        List<Object> objs = JsonUtils.jsonToList(spec.getInputStream());
	        Chainr chainr = Chainr.fromSpec(objs);
	        String result = JsonUtils.toJsonString(chainr.transform(JsonUtils.jsonToMap(input)));
	        return result;
	    } catch (Exception e) {
	        return null;
	    }
	}
///*	
	@Bean
	public int SendInvoiceToAllDebtors() throws Exception{
		Timer t=new Timer();
		t.scheduleAtFixedRate(new TimerTask() {
			  @Override
			  public void run() {
				  
					try {
						System.out.println("Sent email to all debtors ");
						ArrayList<String> emailList= SalesService.GetAllDebtorsEmail();
						for(int i=0;i<emailList.size();i++){
							System.out.println("Sent email to:   "+emailList.get(i));				    
							sendInvoice(emailList.get(i));
						}
						
					} catch ( Exception e) {
						
						e.printStackTrace();
					}
					

			  }
			},30000, 1000*60*60*24);
		
	
		
		return 0;
	}// */
}
