package com.rentit.inventory.application.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.hateoas.ResourceSupport;

import com.rentit.common.application.dto.BusinessPeriodDTO;
//import com.rentit.common.rest.ResourceSupport;
import com.rentit.inventory.domain.model.PlantDeliveryStatus;
import com.rentit.sales.domain.model.POStatus;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data

public class PlantInventoryEntryDTO extends ResourceSupport {
    Long _id;
    String name;
    String description;
    BigDecimal price;
    PlantDeliveryStatus status;
    Long poID;
    
    
}
