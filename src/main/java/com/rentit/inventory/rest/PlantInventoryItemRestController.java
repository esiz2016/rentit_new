package com.rentit.inventory.rest;

import com.rentit.inventory.application.dto.PlantInventoryEntryDTO;
import com.rentit.inventory.application.service.InventoryService;
import com.rentit.inventory.application.service.PlantInventoryEntryAssembler;
import com.rentit.inventory.domain.model.PlantInventoryEntry;
import com.rentit.sales.application.service.SalesService;
import com.rentit.sales.domain.model.PurchaseOrderID;
import com.rentit.sales.infrastructure.idgeneration.SalesIdentifierGenerator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.websocket.DeploymentException;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;;

@RestController
@RequestMapping("/api/inventory/plantItems")
public class PlantInventoryItemRestController {
	
	@Autowired
	SalesService salesservice;
	
	 @Autowired
	 SalesIdentifierGenerator identifierGenerator;
	
	@RequestMapping(method=POST, path="/CreatePlant" )
	public /*PlantInventoryEntryDTO*/Boolean CreatePlantInventoryEntry(@RequestBody PlantInventoryEntryDTO plantDTO) throws DeploymentException
	{
	//	PlantInventoryEntryAssembler pieAssembeler=new PlantInventoryEntryAssembler();
	//	PlantInventoryEntry pie= pieAssembeler.toDataObject(plantDTO);
		
		PlantInventoryEntry pie=	PlantInventoryEntry.of(identifierGenerator.nextPlantInventoryEntryID(),
	   			plantDTO.getName(),
	   			plantDTO.getDescription(),
	   			plantDTO.getPrice(),
	   		/*	PurchaseOrderID.of(  plantDTO.getPoID()), */
	   			plantDTO.getStatus());
	   if	(salesservice.AddNewPlantInventoryEntery(pie)!=null)
		      return true;
	   else
		   return false;
	}
	
	
	
	
}
