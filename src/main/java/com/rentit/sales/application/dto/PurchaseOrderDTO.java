package com.rentit.sales.application.dto;

import com.rentit.common.application.dto.BusinessPeriodDTO;
import com.rentit.common.rest.ResourceSupport;
import com.rentit.customer.dto.AddressDTO;
import com.rentit.customer.dto.CustomerDTO;
import com.rentit.inventory.application.dto.PlantInventoryEntryDTO;
import com.rentit.sales.domain.model.POStatus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@NoArgsConstructor(force = true)
//@AllArgsConstructor
public class PurchaseOrderDTO extends ResourceSupport {
    Long _id;
    PlantInventoryEntryDTO plant;
   // CustomerDTO customer;
    BusinessPeriodDTO rentalPeriod;
    BigDecimal total;
    POStatus status;
    LocalDate issueDate;
    LocalDate paymentSchedule;
    ContactPersonDTO contactPerson;
    AddressDTO address;
}
