package com.rentit.sales.application.dto;

import java.time.LocalDate;

import com.rentit.common.application.dto.BusinessPeriodDTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor(force=true)
@AllArgsConstructor(staticName = "of")
public class ContactPersonDTO {
     String email;
}
